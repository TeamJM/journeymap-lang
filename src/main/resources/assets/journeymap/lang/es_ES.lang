#
# JourneyMap Mod <journeymap.info> for Minecraft
# Copyright (c) 2011-2017  Techbrew Interactive, LLC <techbrew.net>.  All Rights Reserved.
#

### JourneyMap 5.5.0 Resource Bundle - es_ES
### Traducido por: thekazador, Abel, Jaski (hasta v5.2.0) y C4BR3R4 (5.5.X)
### No traducir o cambiar los códigos de color (§b, §e, §f, etc.)
### Los parámetros de posición (%1$s, %2$s, %3$s, etc.) se deberán de reordenar según la estructura semántica de tu idioma
jm.advanced.announcemod=Mensajes de JourneyMap
jm.advanced.announcemod.tooltip=Muestra un mensaje en el chat cuando JourneyMap esta listo.
jm.advanced.automappoll=Actualización de Mapeado automático
jm.advanced.automappoll.tooltip=Frecuencia de actualización del mapeado automático (en milisegundos). Valores bajos harán disminuir el tiempo de actualización, pero podrían empeorar el rendimiento.
jm.advanced.browserpoll=Actualización del Navegador
jm.advanced.browserpoll.tooltip=Frecuencia de actualización del mapa del navegador. Valores bajos harán disminuir el tiempo de actualización, pero podrían empeorar el rendimiento.
jm.advanced.cache_animals=Caché de Animales
jm.advanced.cache_animals.tooltip=Frecuencia de actualización (en milisegundos) de nuevos Animales. Establecer un valor bajo contribuirá con una mayor precisión a cambio de empeorar el rendimiento del juego.
jm.advanced.cache_mobs=Caché de Mobs
jm.advanced.cache_mobs.tooltip=Frecuencia de actualización (en milisegundos) de nuevos Mobs. Establecer un valor bajo contribuirá con una mayor precisión a cambio de empeorar el rendimiento del juego.
jm.advanced.cache_player=Caché del Jugador
jm.advanced.cache_player.tooltip=Frecuencia de actualización (en milisegundos) de Tu jugador. Establecer un valor bajo contribuirá con una mayor precisión a cambio de empeorar el rendimiento del juego.
jm.advanced.cache_players=Caché de Jugadores
jm.advanced.cache_players.tooltip=Frecuencia de actualización (en milisegundos) de nuevos Jugadores. Establecer un valor bajo contribuirá con una mayor precisión a cambio de empeorar el rendimiento del juego.
jm.advanced.cache_villagers=Caché de Aldeanos
jm.advanced.cache_villagers.tooltip=Frecuencia de actualización (en milisegundos) de nuevos Aldeanos. Establecer un valor bajo contribuirá con una mayor precisión a cambio de empeorar el rendimiento del juego.
jm.advanced.checkupdates=Informar sobre actualizaciones
jm.advanced.checkupdates.tooltip=Comprobar si hay disponibles nuevas versiones de JourneyMap.
jm.advanced.loglevel=Nivel de Registro
jm.advanced.loglevel.tooltip=¡Precaución! Algunos niveles de registro pueden empeorar el rendimiento. Se recomienda mantener el valor predeterminado a menos que se indique lo contrario.
jm.advanced.port=Puerto del servidor web
jm.advanced.port.tooltip=Puerto usado para mostrar el mapa en el navegador. Si el puerto ya se encuentra en uso, JourneyMap intentará encontrar otro libre incrementando el valor en 10. (El puerto predeterminado es 8080).
jm.advanced.recordcachestats=Caché del registro de estadísticas
jm.advanced.recordcachestats.tooltip=Activa la memoria caché para los registros de las estadísticas. A no ser que seas un beta-tester se debe mantener desactivado. Activarlo puede empeorar ligeramente el rendimiento del juego.
jm.advanced.tile_render_type=Tipo de Renderizado
jm.advanced.tile_render_type.tooltip=Este ajuste permite optimizar el detalle del mapeado para evitar que las imágenes se vean borrosas. Los valores ajustan el filtro de textura mas el tipo de envoltura: 1=Lineal Reflejado, 2=Lineal Mezclado, 3=Cercano Reflejado, 4=Cercano Mezclado.
jm.colorpalette.basic_colors=Colores Básicos
jm.colorpalette.biome_colors=Colores de Biomas
jm.colorpalette.file_error=El archivo que contiene la paleta de colores no puede ser usado porque contiene errores: %1$s
jm.colorpalette.file_header_1=Este archivo puede ser usado para mantener la paleta de colores consistente cuando se cambian los paquetes de recursos.
jm.colorpalette.file_header_2=Modifícalo por tu propio riesgo. Podrás verlo usando el archivo '%1$s'.
jm.colorpalette.file_header_3=Para utilizar esta paleta de colores en un solo mundo, coloca el archivo '%1$s' en: %2$s
jm.colorpalette.file_header_4=Para utilizar esta paleta de colores en todos los mundos, ajusta en el archivo %1$s "permanent: true" y luego colocalo en: %2$s
jm.colorpalette.file_missing_data=Introduce este archivo en el mismo directorio que %1$s para que se muestre la paleta de colores.
jm.colorpalette.file_title=Paleta de colores de JourneyMap
jm.colorpalette.mods=Mods
jm.colorpalette.resource_packs=Paquetes de Recursos
jm.common.actions=Acciones
jm.common.alwaysmapcaves=Mostrar siempre Cuevas
jm.common.alwaysmapcaves.tooltip=Mostrar siempre el mapa de cuevas, incluso estando sobre la superficie del mundo. Para mejorar el rendimiento se debe mantener desactivado.
jm.common.alwaysmapsurface=Mostrar siempre Superficie
jm.common.alwaysmapsurface.tooltip=Mostrar siempre el mapa de la superficie del mundo, incluso estando bajo tierra. Para mejorar el rendimiento se debe mantener desactivado.
jm.common.automap_complete=Se ha detenido el mapeado automático de la superficie.
jm.common.automap_complete_underground=Se ha detenido el mapeado automático de la parte %1$s
jm.common.automap_dialog=Mapeado Automático
jm.common.automap_dialog_summary_1=Nota: Desactivar las siguientes opciones de la Cartografía, mejorará los resultados:
jm.common.automap_dialog_summary_2='Mezclar Hojas', 'Mezclar Césped' y 'Mezclar Agua'.
jm.common.automap_dialog_all=Todo el Mundo
jm.common.automap_dialog_missing=Lo que falta
jm.common.automap_dialog_text=¿Renderizar automáticamente todo el mundo o solo las partes que faltan?
jm.common.automap_status=Mapeando automáticamente la superficie. Progreso: %1$s
jm.common.automap_status_underground=Mapeando automáticamente la región %1$s. Progreso: %2$s
jm.common.automap_text=Renderiza automáticamente todas las regiones del mapa (solo en modo Solitario)
jm.common.automap_title=Mapeado Automático...
jm.common.automap_region_start=Renderizando...
jm.common.automap_region_complete=Renderizado
jm.common.automap_region_chunks=Chunks mapeados: %1$s
jm.common.automap_stop_text=Detener el mapeado automático al explorar el mapa
jm.common.automap_stop_title=Detener el mapeado automático
jm.common.birthday=¡Felicidades %1$s!
jm.common.chat_announcement=§7[JourneyMap] §e%1$s
jm.common.close=Cerrar
jm.common.day=Día
jm.common.colorreset_text=Recalcular el color de todos los bloques del mapa
jm.common.colorreset_title=Restaurar la paleta de colores
jm.common.colorreset_start=Recalculando paleta de colores...
jm.common.colorreset_complete=Paleta de colores recalculada.
jm.common.deletemap_dialog=Borrar mapa
jm.common.deletemap_dialog_all=Todas las dimensiones
jm.common.deletemap_dialog_text=¿Eliminar el mapa de todas las dimensiones o sólo la de esta dimensión?
jm.common.deletemap_dialog_this=Esta dimensión
jm.common.deletemap_status_done=Mapa eliminado.
jm.common.deletemap_status_error=No es posible eliminar el mapa. Revise journeymap.log para obtener más detalles.
jm.common.deletemap_text=Eliminar las imágenes del mapa renderizado de una o más dimensiones.
jm.common.deletemap_title=Eliminar Renderizado del Mapa...
jm.common.dimension=Dimensión %1$s
jm.common.disabled_feature=Característica no disponible
jm.common.discord=Discord de JourneyMap
jm.common.discord.tooltip=Entra en nuestra comunidad para recibir ayuda o hablar.
jm.common.download=Lista de cambios y descargas
jm.common.download.tooltip=Obtén las últimas versiones o betas en Curseforge.com
jm.common.enable=Habilitado
jm.common.enable_mapping_false=Desactivar mapeado
jm.common.enable_mapping_false_text=Mapeado desactivado.
jm.common.enable_mapping_true=Activar Mapeado
jm.common.enable_mapping_true_text=Mapeado activado.
jm.common.entity_display.large_dots=Puntos Grandes
jm.common.entity_display.small_dots=Puntos Pequeños
jm.common.entity_display.large_icons=Iconos Grandes
jm.common.entity_display.small_icons=Iconos Pequeños
jm.common.file_updates=Archivos actualizados (%1$s). Mapeado cancelado.
jm.common.file_updates_complete=Archivos actualizados. Mapeado completado.
jm.common.font_scale=Escala del Texto
jm.common.font_scale.tooltip=Escala del texto para las marcas y etiquetas.
jm.common.grid_edit=Estilo del Chunk...
jm.common.grid_edit.tooltip=Cambia la apariencia del chunk en todos los mapas.
jm.common.grid_editor=Editor de Estilo de Chunk
jm.common.grid_opacity=Opacidad
jm.common.grid_style=Forma
jm.common.grid_style_checkers=Ajedrez
jm.common.grid_style_dots=Puntos
jm.common.grid_style_squares=Cuadrados
jm.common.help=Ayuda...
jm.common.hide_buttons=Esconder botones
jm.common.hotkeys=Teclas rápidas...
jm.common.hotkeys.tooltip=Ver qué teclas rápidas se utilizan en este mapa.
jm.common.hotkeys_keybinding_category=MiniMapa de JourneyMap
jm.common.hotkeys_keybinding_fullscreen_category=Mapa Completo de JourneyMap
jm.common.location_format=Ubicación
jm.common.location_format.tooltip=Formato de visualización de las coordenadas de ubicación. La 'v' representa el tramo del chunk vertical.
jm.common.location_format_verbose=Mostrar Coordenadas Cartesianas
jm.common.location_format_verbose.tooltip=Muestra las coordenadas cartesianas representadas por las letras x,y,z y no solo por números.
jm.common.location_xyvz_label=x, y (v), z
jm.common.location_xyvz_plain=%1$s, %3$s (%4$s), %2$s
jm.common.location_xyvz_verbose=x: %1$s, y: %3$s (%4$s), z: %2$s
jm.common.location_xyz_label=x, y, z
jm.common.location_xyz_plain=%1$s, %3$s, %2$s
jm.common.location_xyz_verbose=x: %1$s, y: %3$s, z: %2$s
jm.common.location_xz_label=x, z
jm.common.location_xz_plain=%1$s, %2$s
jm.common.location_xz_verbose=x: %1$s, z: %2$s
jm.common.location_xzy_label=x, z, y
jm.common.location_xzy_plain=%1$s, %2$s, %3$s
jm.common.location_xzy_verbose=x: %1$s, z: %2$s, y: %3$s
jm.common.location_xzyv_label=x, z, y (v)
jm.common.location_xzyv_plain=%1$s, %2$s, %3$s (%4$s)
jm.common.location_xzyv_verbose=x: %1$s, z: %2$s, y: %3$s (%4$s)
jm.common.log_warning=El nivel de registro %1$s puede ralentizar el juego. ¡No usar para jugar!
jm.common.map_saved=El Mapa se a guardado en '%1$s'
jm.common.map_style_antialiasing=Usar Antialiasing
jm.common.map_style_antialiasing.tooltip=Suaviza el contorno de los bordes de las sombras. Desactivar el suavizado puede mejorar el rendimiento.
jm.common.map_style_bathymetry=Muestra la Batimetría
jm.common.map_style_bathymetry.tooltip=Muestra el terreno situado bajo el fondo oceánico.
jm.common.map_style_blendgrass=Mezclar Césped
jm.common.map_style_blendgrass.tooltip=Mezcla el color del césped entre biomas. Desactivar para mejorar el rendimiento.
jm.common.map_style_blendfoliage=Mezclar Hojas
jm.common.map_style_blendfoliage.tooltip=Mezcla el color de las hojas entre biomas. Desactivar para mejorar el rendimiento.
jm.common.map_style_blendwater=Mezclar Agua
jm.common.map_style_blendwater.tooltip=Mezcla el color del agua entre biomas. Desactivar para mejorar el rendimiento.
jm.common.map_style_topography=Activar Mapa Topográfico
jm.common.map_style_topography.tooltip=Tipo de mapa que muestra los cambios de nivel a base de líneas.
jm.common.map_style_button=Estilo de mapa...
jm.common.map_style_caveignoreglass=Ignorar techos de cristal
jm.common.map_style_caveignoreglass.tooltip=Permanecer bajo un techo de cristal no hará cambiar el mapa a modo cueva.
jm.common.map_style_cavelighting=Usar iluminación de cueva
jm.common.map_style_cavelighting.tooltip=Usar el nivel de iluminación subterránea. Al desactivar esta función se obtiene una iluminación completa.
jm.common.map_style_caveshowsurface=Muestra la superficie sobre la cueva
jm.common.map_style_caveshowsurface.tooltip=Si esta función está activada, cuando permanezcas bajo tierra, la superficie se mostrará como una zona oscura.
jm.common.map_style_crops=Mostrar Plantaciones
jm.common.map_style_crops.tooltip=Muestra las plantaciones en el mapa.
jm.common.map_style_plants=Mostrar Plantas
jm.common.map_style_plants.tooltip=Muestra las plantas en el mapa.
jm.common.map_style_plantshadows=Sombras de Plantas y Plantaciones
jm.common.map_style_plantshadows.tooltip=Muestra las sombras proyectadas por las Plantas y las Plantaciones.
jm.common.map_style_title=Configuración del Estilo del Mapa
jm.common.map_style_transparency=Usar Transparencias
jm.common.map_style_transparency.tooltip=Los bloques transparentes mostrarán lo que está por debajo de ellos.
jm.common.mapgui_only_ready=Presiona '§b%1$s§f'
jm.common.memory_warning=Minecraft se esta quedando sin memoria: %1$s
jm.common.mob_icon_set=Iconos de los Mobs
jm.common.mob_icon_set.tooltip=Conjunto de iconos usado para mostrar los mobs en el MiniMapa.
jm.common.new_version_available=Nueva versión disponible: %1$s
jm.common.night=Noche
jm.common.off=Desactivado
jm.common.on=Activado
jm.common.options=Configuración
jm.common.options_button=Configuración...
jm.common.patreon=Ayuda a JourneyMap
jm.common.patreon.tooltip=¿Te gusta JourneyMap? ¡Conviértete en patrocinador y ayuda a su desarrollo!
jm.common.radar_hide_sneaking=Ocultar jugadores agachados
jm.common.radar_hide_sneaking.tooltip=Activar esta función hará que no se muestren los jugadores que permanezcan agachados.
jm.common.radar_lateral_distance=Radio horizontal
jm.common.radar_lateral_distance.tooltip=Distancia horizontal (en bloques) para mostrar entidades en el radar. Establecer un valor alto puede empeorar el rendimiento.
jm.common.radar_max_animals=Cantidad de Animales
jm.common.radar_max_animals.tooltip=Número máximo de animales que serán mostrados en el radar. Establecer un valor alto puede empeorar el rendimiento.
jm.common.radar_max_mobs=Cantidad de Monstruos
jm.common.radar_max_mobs.tooltip=Número máximo de monstruos que serán mostrados en el radar. Establecer un valor alto puede empeorar el rendimiento.
jm.common.radar_max_players=Cantidad de Jugadores
jm.common.radar_max_players.tooltip=Número máximo de jugadores que serán mostrados en el radar. Establecer un valor alto puede empeorar el rendimiento.
jm.common.radar_max_villagers=Cantidad de Aldeanos
jm.common.radar_max_villagers.tooltip=Número máximo de aldeanos que serán mostrados en el radar. Establecer un valor alto puede empeorar el rendimiento.
jm.common.radar_vertical_distance=Radio vertical
jm.common.radar_vertical_distance.tooltip=Distancia vertical (en bloques) para mostrar entidades en el Radar. Establecer un valor alto puede empeorar el rendimiento.
jm.common.ready=§e%1$s listo.
jm.common.renderdelay=Latencia de Renderizado
jm.common.renderdelay.tooltip=Tiempo (en segundos) que se ha de esperar entre cada tarea renderizado del mapa. Un valor alto puede mejorar el rendimiento, pero también puede hacer que no se rendericen algunos chunks al moverte.
jm.common.renderdistance_cave_max=Distancia máxima en Cuevas
jm.common.renderdistance_cave_max.tooltip=Distancia (en chunks) alrededor tuyo, que serán mapeados bajo tierra o en dimensiones sin cielo. Valores bajos pueden mejorar el rendimiento. Valores más altos que los del render propio de Minecraft no tendrán efecto.
jm.common.renderdistance_cave_min=Distancia mínima en Cuevas
jm.common.renderdistance_cave_min.tooltip=Distancia (en chunks) alrededor tuyo que siempre serán mapeados bajo tierra o en dimensiones sin cielo. Valores bajos pueden mejorar el rendimiento. Valores más altos que los del render propio de Minecraft no tendrán efecto.
jm.common.renderdistance_surface_max=Distancia máxima en Superficie
jm.common.renderdistance_surface_max.tooltip=Distancia (en chunks) alrededor tuyo serán mapeados en la superficie de dimensiones con cielo. Valores bajos pueden mejorar el rendimiento. Valores más altos que los del render propio de Minecraft no tendrán efecto.
jm.common.renderdistance_surface_min=Distancia mínima en Superficie
jm.common.renderdistance_surface_min.tooltip=Distancia (en chunks) alrededor tuyo que siempre serán mapeados en la superficie de dimensiones con cielo. Valores bajos pueden mejorar el rendimiento. Valores más altos que los del render propio de Minecraft no tendrán efecto.
jm.common.renderstats=Ultimos %1$s (%2$s + %3$s) chunks en %4$sms (%5$sms de media)
jm.common.renderstats.title=Estadística del último renderizado
jm.common.renderstats.tooltip=Chunks totales = (Todos los chunks a mínima distancia + algunos a máxima distancia). El terreno de tu alrededor y la configuración cartográfica, pueden cambiar estos datos significativamente.
jm.common.renderstats_debug_cave=Cueva: %1$s (%2$s) + %3$s (%4$s) = %5$s chunks en %6$sms (%7$sms de media)
jm.common.renderstats_debug_cave_simple=Cueva: %1$s = %2$s chunks en %3$sms (%4$sms de media)
jm.common.renderstats_debug_surface=Superficie: %1$s (%2$s) + %3$s (%4$s) = %5$s chunks en %6$sms (%7$sms de media)
jm.common.renderstats_debug_surface_simple=Superficie: %1$s = %2$s chunks en %3$sms (%4$sms de media)
jm.common.renderstats_debug_topo=Topográfico: %1$s (%2$s) + %3$s (%4$s) = %5$s chunks en %6$sms (%7$sms de media)
jm.common.renderstats_debug_topo_simple=Topográfico: %1$s = %2$s chunks en %3$sms (%4$sms de media)
jm.common.revealshape=Revelar en forma de
jm.common.revealshape.tooltip=Revela la forma del mapa alrededor tuyo. El círculo muestra menos chunks que el formato cuadrado, por lo que puede mejorar el rendimiento.
jm.common.save_filename=Guardando el Mapa en el archivo de imagen '%1$s'
jm.common.save_map=Guardar Mapa
jm.common.saving_map_to_file=Guardando el mapa %1$s en un archivo...
jm.common.show_animals=Mostrar Animales
jm.common.show_animals.tooltip=Mostrar en el mapa los animales cercanos.
jm.common.show_buttons=Mostrar Botones
jm.common.show_caves=Cambiar al mapa de Cuevas
jm.common.show_caves.tooltip=Cambiar automáticamente al mapa de cuevas cuando te encuentras bajo tierra o en el interior de alguna construcción.
jm.common.show_day_night=Cambiar entre Día y Noche
jm.common.show_day_night.tooltip=Cambia el mapa automáticamente entre el Día y la Noche.
jm.common.show_entity_names=Mostrar Nombre de Entidades
jm.common.show_entity_names.tooltip=Mostrar en el mapa los Nombres de las Mascotas, NPCs, etc.
jm.common.show_grid=Mostrar Chunks
jm.common.show_grid.tooltip=Mostrar en el mapa una serie de cuadros que indican donde esta situado cada chunk.
jm.common.show_grid_shift.tooltip=Mantén la tecla 'Shift' al clicar para configurar.
jm.common.show_keys=Mostrar Asignaciones
jm.common.show_keys.tooltip=Mostrar una leyenda sobre el mapa, donde se muestren las teclas asignadas y sus descripciones.
jm.common.show_mobs=Mostrar Monstruos
jm.common.show_mobs.tooltip=Mostrar en el mapa los monstruos cercanos.
jm.common.show_pets=Mostrar Mascotas
jm.common.show_pets.tooltip=Mostrar en el mapa las mascotas cercanas.
jm.common.show_players=Mostrar Jugadores
jm.common.show_players.tooltip=Mostrar en el mapa los jugadores y NPCs cercanos.
jm.common.show_self=Mostrarte a tí en el Mapa
jm.common.show_self.tooltip=Mostrar en el mapa tu icono de ubicación.
jm.common.show_villagers=Mostrar Aldeanos
jm.common.show_villagers.tooltip=Mostrar en el mapa los aldeanos cercanos.
jm.common.show_waypoints=Mostrar Marcas
jm.common.show_waypoints.tooltip=Mostrar en el mapa las Marcas de Posición.
jm.common.splash_about=A cerca de...
jm.common.splash_action=Clic para mostrar
jm.common.splash_developer=Desarrollador
jm.common.splash_friend=Amigo
jm.common.splash_patreon=Patrocinador
jm.common.splash_tester=Testeador
jm.common.splash_icons=Iconos
jm.common.splash_title=Sobre JourneyMap %1$s
jm.common.splash_walloffame=Muro de la fama
jm.common.splash_whatisnew=¿Qué hay nuevo en esta versión?
jm.common.tile_display_quality=Alta Calidad
jm.common.tile_display_quality.tooltip=Recomendado para usar con 'Orientación del MiniMapa > Tu Orientación'. No mejora la calidad del renderizado, solo en cómo se muestra el mapa. Desactivar para mejorar rendimiento de Minecraft.
jm.common.ui_theme=Tema
jm.common.ui_theme.tooltip=Tema de la interfaz, el Mapa completo y el MiniMapa.
jm.common.ui_theme_applied=Tema establecido. Dirígete a §bConfiguración > Diseño de MiniMapa #§f para ajustar la apariencia del MiniMapa.
jm.common.ui_theme_author=Autor: §b%1$s§f
jm.common.ui_theme_name=Nombre: §b%1$s§f
jm.common.update_available=¡Actualización disponible!
jm.common.update_check=Buscar actualizaciones
jm.common.use_browser=Abrir mapa en el navegador
jm.common.webserver_and_mapgui_disabled=JourneyMap esta desactivado a causa del archivo jm.common.properties
jm.common.webserver_and_mapgui_ready=Pulsa la tecla §b%1$s§f para abrir el mapa en el juego o abre el mapa en tu navegador usando la dirección §b§nhttp://localhost%2$s§f
jm.common.webserver_only_ready=Abre el mapa en el navegador usando el enlace §b§nhttp://localhost%1$s§f. El mapa del juego está desactivado. 
jm.common.website=Artículos de ayuda y procedimientos de uso de JourneyMap
jm.config.category.advanced=Configuración Avanzada
jm.config.category.advanced.tooltip=¡Precaucion! Cambiar alguno de estos valores podría causar problemas significativos y/o accidentes.
jm.config.category.cartography=Cartografía
jm.config.category.cartography.tooltip=Configura cómo son renderizadas las imágenes del mapa.
jm.config.category.fullmap=Mapa Completo
jm.config.category.fullmap.tooltip=Configura la apariencia del Mapa a Pantalla Completa.
jm.config.category.minimap=Diseño de MiniMapa 1
jm.config.category.minimap.tooltip=Configura la apariencia del MiniMapa predeterminado 1. (Alterna entre ambos estilos de MiniMapa, pulsando la tecla '\').
jm.config.category.minimap2=Diseño de MiniMapa 2
jm.config.category.minimap2.tooltip=Configura la apariencia del MiniMapa predeterminado 2. (Alterna entre ambos estilos de MiniMapa, pulsando la tecla '\').
jm.config.category.waypoint=Marcas de Posición
jm.config.category.waypoint.tooltip=Configura como deben mostrarse las Marcas de posición.
jm.config.category.waypoint_beacons=Balizas e Iconos
jm.config.category.waypoint_beacons.tooltip=Configura como deben mostrarse las balizas y los iconos en el juego.
jm.config.category.webmap=Mapa del Navegador
jm.config.category.webmap.tooltip=Configura la apariencia del mapa mostrado en el navegador.
jm.config.control_arrowkeys=Usa las flechas del teclado para ajustar el valor con mayor precisión.
jm.config.default=[Predeterminado: %1$s]
jm.config.default_numeric=[Distancia: %1$d ~ %2$d, Predeterminado: %3$d]
jm.config.file_header_1=Configuración del archivo JourneyMap. Modifícalo por tu propio riesgo.
jm.config.file_header_2=Para usar en todos los mundos, pon aquí: %1$s
jm.config.file_header_3=Para anular la configuración en un mundo, pon aquí: %1$s(nombre del mundo)
jm.config.file_header_4=Para restaurar la configuración predeterminada, simplemente borra este archivo antes de iniciar Minecraft
jm.config.file_header_5=Para mas información, dirígete a: %1$s
jm.config.reset=Restablecer
jm.config.reset.tooltip=Restablece la configuración de esta sección por el valor predeterminado.
jm.config.tooltip_format=%1$s%2$s
jm.error.compatability=Errores de incompatibilidad corriendo %1$s por Forge %2$s. Asegúrese de que su versión de Forge sea compatible.
jm.error.java6=JourneyMap necesita Java 7 o Java 8. Configura tu perfil en el lanzador de Minecraft para usar una versión más reciente de Java.
jm.feature.fairplay=Juego limpio
jm.feature.unlimited=Ilimitado
jm.fullscreen.follow=Centrar mapa en tu posición
jm.fullscreen.hotkeys_doubleclick=(doble clic)
jm.fullscreen.hotkeys_east=Pan Este (16 bloques)
jm.fullscreen.hotkeys_north=Pan Norte (16 bloques)
jm.fullscreen.hotkeys_south=Pan Sur (16 bloques)
jm.fullscreen.hotkeys_title=Atajos del mapa en pantalla completa
jm.fullscreen.hotkeys_waypoint=Crear/editar Marcas
jm.fullscreen.hotkeys_west=Pan Oeste (16 bloques)
jm.fullscreen.map_caves=Mapa de Cuevas
jm.fullscreen.map_cave_layers=Mapa de Cuevas
jm.fullscreen.map_cave_layers.button=Profundidad:
jm.fullscreen.map_cave_layers.button.tooltip=De la capa %1$s a la %2$s
jm.fullscreen.map_day=Mapa Diurno
jm.fullscreen.map_night=Mapa Nocturno
jm.fullscreen.map_topo=Mapa Topográfico
jm.fullscreen.minimap=MiniMapa...
jm.fullscreen.title=Mapa Completo
jm.fullscreen.zoom_in=Acercar Mapa
jm.fullscreen.zoom_out=Alejar Mapa
jm.minimap.compass.e=E
jm.minimap.compass.n=N
jm.minimap.compass.s=S
jm.minimap.compass.w=O
jm.minimap.compass_font_scale=Tamaño de los Puntos Cardinales
jm.minimap.compass_font_scale.tooltip=Tamaño del texto para mostrar los puntos cardinales (N,S,E,O) en el MiniMapa.
jm.minimap.enable_minimap=Activar MiniMapa
jm.minimap.enable_minimap.tooltip=Mostrar el MiniMapa en el juego.
jm.minimap.frame_alpha=Opacidad del Marco
jm.minimap.frame_alpha.tooltip=Ajusta (en porcentaje) la transparencia del marco que rodea el MiniMapa.
jm.minimap.hotkeys=Activar asignaciones en el MiniMapa
jm.minimap.hotkeys.tooltip=Activar las asignaciones de teclas en el MiniMapa.
jm.minimap.hotkeys_title=Asignaciones del MiniMapa
jm.minimap.hotkeys_toggle=Cambiar el Diseño de MiniMapa
jm.minimap.mob_display=Mostrar Mobs
jm.minimap.mob_display.tooltip=Muestra los mobs como puntos o iconos.
jm.minimap.mob_heading=Mostrar Orientación de Mobs
jm.minimap.mob_heading.tooltip=Mostrar la dirección hacia donde miran los mobs. Solo si se muestran como puntos.
jm.minimap.options=Configuración del MiniMapa
jm.minimap.orientation.button=Orientación del MiniMapa
jm.minimap.orientation.button.tooltip=El Norte se sitúa hacia arriba, el Norte Antiguo se sitúa hacia abajo y Tu Orientación hace que el mapa gire según tu orientación (que solo funciona con el MiniMapa en Forma de Círculo).
jm.minimap.orientation.compass=Puntos Cardinales
jm.minimap.orientation.north=Norte
jm.minimap.orientation.oldnorth=Norte Antiguo
jm.minimap.orientation.playerheading=Tu Orientación
jm.minimap.player_display=Mostrar Jugadores
jm.minimap.player_display.tooltip=Muestra los jugadores como Puntos o como Iconos.
jm.minimap.player_heading=Mostrar Orientación de Jugadores
jm.minimap.player_heading.tooltip=Mostrar la dirección hacia donde miran los jugadores. Solo si se muestran como puntos.
jm.minimap.position=Posición
jm.minimap.position.tooltip=Cambia la posición del MiniMapa en la pantalla.
jm.minimap.position_bottomleft=Inferior Izquierda
jm.minimap.position_bottomright=Inferior Derecha
jm.minimap.position_center=Centrado
jm.minimap.position_topcenter=Superior Centrado
jm.minimap.position_topleft=Superior Izquierda
jm.minimap.position_topright=Superior Derecha
jm.minimap.preview=Previsualizar
jm.minimap.preview.tooltip=Muestra en tiempo real los ajustes en la configuración del MiniMapa. Para mostrar la configuración del otro estilo puedes usar la tecla '\'. Para terminar puedes deseleccionar esta casilla o pulsar la tecla 'Esc'.
jm.minimap.reticle_orientation=Orientación de la Retícula
jm.minimap.reticle_orientation.tooltip=Orientación (o rotación) de las líneas que permiten precisar la visual en el MiniMapa.
jm.minimap.return_to_game=Volver al juego
jm.minimap.shape=Forma
jm.minimap.shape.tooltip=Forma del MiniMapa.
jm.minimap.shape_circle=Círculo
jm.minimap.shape_rectangle=Rectángulo
jm.minimap.shape_square=Cuadrado
jm.minimap.show_biome=Mostrar Bioma
jm.minimap.show_biome.tooltip=Muestra el nombre del bioma de donde te encuentras.
jm.minimap.show_compass=Mostrar Puntos Cardinales
jm.minimap.show_compass.tooltip=Muestra los puntos cardinales (N, S, E y O) alrededor del borde del MiniMapa.
jm.minimap.show_fps=Mostrar los FPS
jm.minimap.show_fps.tooltip=Muestra los FPS (fotogramas por segundo) actuales.
jm.minimap.show_location=Mostrar la ubicación
jm.minimap.show_location.tooltip=Muestra su ubicación actual.
jm.minimap.show_reticle=Mostrar Retícula
jm.minimap.show_reticle.tooltip=Muestra una cruz en el MiniMapa para precisar la orientación.
jm.minimap.show_waypointlabels=Mostrar nombre de Marca
jm.minimap.show_waypointlabels.tooltip=Muestra los nombres de las marcas. Si no está activado solo se muestra el icono.
jm.minimap.size=Tamaño del MiniMapa
jm.minimap.size.tooltip=Ajusta el tamaño del MiniMapa (en porcentaje del tamaño de la ventana). Establecer un tamaño superior a 768px puede empeorar rendimiento del juego.
jm.minimap.terrain_alpha=Opacidad del MiniMapa
jm.minimap.terrain_alpha.tooltip=Ajusta (en porcentaje) la transparencia del MiniMapa.
jm.minimap.texture_size=Iconos Pequeños
jm.minimap.texture_size.tooltip=Usa iconos pequeños en el Mapa.
jm.minimap.title=MiniMapa
jm.minimap.info1_label.button=Información 1
jm.minimap.info1_label.button.tooltip=Muestra información en la posición 1.
jm.minimap.info2_label.button=Información 2
jm.minimap.info2_label.button.tooltip=Muestra información en la posición 2.
jm.minimap.info3_label.button=Información 3
jm.minimap.info3_label.button.tooltip=Muestra información en la posición 3.
jm.minimap.info4_label.button=Información 4
jm.minimap.info4_label.button.tooltip=Muestra información en la posición 4.
jm.theme.labelsource.biome=Bioma
jm.theme.labelsource.biome.tooltip=Muestra el nombre del bioma del lugar donde te encuentras.
jm.theme.labelsource.blank=Nada
jm.theme.labelsource.blank.tooltip=No mostrar nada.
jm.theme.labelsource.fps=FPS
jm.theme.labelsource.fps.tooltip=Muestra tu FPS (fotogramas por segundo) actuales.
jm.theme.labelsource.gametime=Hora del Juego
jm.theme.labelsource.gametime.tooltip=Muestra la hora del mundo (20 minutos por ciclo).
jm.theme.labelsource.gametime.day=Día
jm.theme.labelsource.gametime.sunrise=Amanecer
jm.theme.labelsource.gametime.sunset=Puesta de Sol
jm.theme.labelsource.gametime.night=Noche
jm.theme.labelsource.realtime=Hora del Sistema
jm.theme.labelsource.realtime.tooltip=Muestra la hora del PC.
jm.theme.labelsource.location=Localización
jm.theme.labelsource.location.tooltip=Muestra las coordenadas de tu posición.
jm.waypoint.auto_hide_label=Ocultar si no se mira
jm.waypoint.auto_hide_label.tooltip=Oculta el nombre de la Marca cuando no se está mirando hacia ella. De lo contrario siempre serán visibles.
jm.waypoint.blue_abbreviated=B
jm.waypoint.bold_label=Nombre en Negrita
jm.waypoint.bold_label.tooltip=Utilizar negrita en las etiquetas de las Marcas de posición.
jm.waypoint.cancel=Cancelar
jm.waypoint.chat=Publicar
jm.waypoint.chat.tooltip=Inserta tus coordenadas de posición en el chat.
jm.waypoint.color=Color
jm.waypoint.create_deathpoints=Crear puntos de Muerte
jm.waypoint.create_deathpoints.tooltip=Crea automáticamente una Marca en el lugar donde mueres (indicando además la hora y la fecha de la misma).
jm.waypoint.current_location=Ubicación actual: %1$s
jm.waypoint.deathpoint=Muerte
jm.waypoint.dimension=Dimensión: %1$s
jm.waypoint.dimension_all=Todas
jm.waypoint.dimensions=Dimensiones
jm.waypoint.display_management=Muestra y administra las Marcas en el juego
jm.waypoint.distance=Distancia
jm.waypoint.distance_meters=(está a %1$s bloques)
jm.waypoint.edit=Editar
jm.waypoint.edit_title=Editar Marcas
jm.waypoint.enable=Habilitado: %1$s
jm.waypoint.enable_all=%2$s las %1$s Marcas 
jm.waypoint.enable_beacons=Mostrar Marcas en el Juego
jm.waypoint.enable_beacons.tooltip=Muestra las Marcas en el juego.
jm.waypoint.enable_manager=Activa la administración de las Marcas
jm.waypoint.enable_manager.tooltip=Si usas otro mod que administre las Marcas, deberías desactivar esta función.
jm.waypoint.find=Encontrar
jm.waypoint.font_scale=Tamaño del texto de la Marca.
jm.waypoint.font_scale.tooltip=Tamaño del texto usado para mostrar los nombres de las marcas en el juego.
jm.waypoint.green_abbreviated=G
jm.waypoint.help_create_ingame=Crear/editar Marca (en el juego)
jm.waypoint.help_create_inmap=Crear/editar Marca (en el mapa)
jm.waypoint.help_import_rei=O copiar §b%1$s§f a §b%2$s§f y reinicie Minecraft.
jm.waypoint.help_import_rei_title=Importar Marcas desde Rei's MiniMap
jm.waypoint.help_import_voxel=O copiar §b%1$s§f a §b%2$s§f y reinicie Minecraft.
jm.waypoint.help_import_voxel_title=Importar Marcas desde VoxelMap
jm.waypoint.help_manage_ingame=Administrar los Marcas (en el juego)
jm.waypoint.help_title=Ayuda de Marca
jm.waypoint.import_rei_errors=Se han importado %1$s Marcas del mod Rei's MiniMap con %2$s errores. Observa el archivo journeymap.log para obtener más detalles.
jm.waypoint.import_rei_failure=No se han podido importar las Marcas del mod Rei's MiniMap. Observa el archivo journeymap.log para obtener más detalles.
jm.waypoint.import_rei_file_error=No se ha podido importar el archivo de Marcas '%2$s' del mod Rei's MiniMap
jm.waypoint.import_rei_success=Se han importado %1$s Marcas del mod Rei's Minimap.
jm.waypoint.import_rei_version=Tu versión del mod Rei's MiniMap no es compatible.
jm.waypoint.import_vox_errors=Se han importado %1$s Marcas del mod VoxelMap con %1$s errores. Observa el archivo journeymap.log para obtener más detalles.
jm.waypoint.import_vox_failure=No se han podido importar las Marcas del mod VoxelMap. Observa el archivo journeymap.log para obtener más detalles.
jm.waypoint.import_vox_file_error=No se ha podido importar el archivo de Marcas '%1$s' del mod VoxelMap
jm.waypoint.import_vox_success=Se han importado %1$s Marcas del mod VoxelMap.
jm.waypoint.import_vox_version=Tu versión del mod VoxelMap no es compatible. Por favor, actualiza a la versión
jm.waypoint.location=Ubicación
jm.waypoint.manage_title=Administración de Marcas
jm.waypoint.manager=Administrar Marcas de Posición
jm.waypoint.max_distance=Distancia Máxima
jm.waypoint.max_distance.tooltip=Distancia máxima (en bloques) que debe haber entre el jugador y la marca para que esta sea mostrada. Para que la marca siempre sea mostrada hay que establecer el valor en 0.
jm.waypoint.min_distance=Distancia Mínima
jm.waypoint.min_distance.tooltip=Distancia mínima (en bloques) que debe haber entre el jugador y la marca para que esta sea mostrada. Para que la marca siempre sea mostrada hay que establecer el valor en 0.
jm.waypoint.name=Nombre
jm.waypoint.new=Nueva...
jm.waypoint.new_title=Nueva Marca de Posición
jm.waypoint.options=Opciones de Marcas (Visualización en el juego)
jm.waypoint.randomize=Color aleatorio
jm.waypoint.red_abbreviated=R
jm.waypoint.remove=Eliminar
jm.waypoint.reset=Restablecer
jm.waypoint.save=Guardar
jm.waypoint.show_distance=Mostrar la Distancia
jm.waypoint.show_distance.tooltip=Mostrar la distancia (en bloques) que hay desde donde estás hasta la posición de la Marca.
jm.waypoint.show_name=Mostrar el Nombre
jm.waypoint.show_name.tooltip=Mostrar el nombre de la Marca.
jm.waypoint.show_rotating_beam=Rotación de Baliza
jm.waypoint.show_rotating_beam.tooltip=Utilizar un haz giratorio para mostrar la Baliza. NOTA: Si quieres desactivar la Baliza y mostrar únicamente la Marca, deberás desactivar esta función junto con la función 'Baliza Estática'.
jm.waypoint.show_static_beam=Baliza Estática
jm.waypoint.show_static_beam.tooltip=Utilizar un haz estático para mostrar la Baliza. NOTA: Si quieres desactivar la Baliza y mostrar únicamente la Marca, deberás desactivar esta función junto con la función 'Rotación de Baliza'.
jm.waypoint.show_texture=Mostrar Icono
jm.waypoint.show_texture.tooltip=Mostrar el icono de la Marca.
jm.waypoint.teleport=Aparecer
jm.waypoint.texture_size=Icono Pequeño
jm.waypoint.texture_size.tooltip=Usar iconos pequeños para las marcas.
jm.waypoint.waypoints=Marcas
jm.waypoint.waypoints_button=Marcas...
jm.waypoint.x=X
jm.waypoint.y=Y
jm.waypoint.z=Z
jm.webmap.actions_title=Acciones...
jm.webmap.animals_menu_item_text=Animales
jm.webmap.animals_menu_item_title=Mostrar animales cercanos
jm.webmap.automap_already_started=Mapeado automático ya en proceso.
jm.webmap.automap_complete=El mapeado automático se ha detenido.
jm.webmap.automap_complete_underground=El mapeado automático de la parte %1$s se ha detenido.
jm.webmap.automap_dialog_all=Todo el Mapa
jm.webmap.automap_dialog_close=Cerrar Mapa
jm.webmap.automap_dialog_missing=Lo que Falta
jm.webmap.automap_dialog_none=Nada
jm.webmap.automap_dialog_text=¿Asignar automáticamente todas las regiones o sólo las regiones que faltan?
jm.webmap.automap_started=Mapeado automático iniciado. Compruebe el chat del juego para ver el progreso.
jm.webmap.automap_text=Mapeado automático...
jm.webmap.automap_title=Mapear automáticamente todas las regiones no exploradas del mapa (un sólo jugador).
jm.webmap.biome_text=Bioma:
jm.webmap.caves_menu_item_text=Cuevas
jm.webmap.caves_menu_item_title=Mostrar cuevas/interiores cuando no estás bajo el cielo.
jm.webmap.day_button_text=Día
jm.webmap.day_button_title=Mapa en modo diurno.
jm.webmap.donate_text=Ayuda a JourneyMap
jm.webmap.donate_title=realiza una donación para ayudar al desarrollo de JourneyMap usando Patreon.
jm.webmap.elevation_text=Altura:
jm.webmap.email_sub_text=Recibir noticias de JourneyMap por e-mail
jm.webmap.email_sub_title=Suscribirse a las noticias de JourneyMap por e-mail
jm.webmap.enable=Activar Navegador
jm.webmap.enable.tooltip=Inicia el servidor web incorporado, que permite mostrar el mapa en el Navegador.
jm.webmap.error_world_not_opened=El mundo de Minecraft que estabas viendo de ha cerrado. Prueba pulsando F5.
jm.webmap.follow_button_title=Centra el mapa en tu posición cuando te mueves. El seguimiento se desactiva si mueves la posición del mapa.
jm.webmap.follow_twitter_text=Seguir en Twitter
jm.webmap.follow_twitter_title=Seguir a JourneyMap en Twitter
jm.webmap.forum_link_text=minecraftforum.net
jm.webmap.forum_link_title=Foro de JourneyMap en minecraftforum.net
jm.webmap.google_domain=Dominio API de Google Maps
jm.webmap.google_domain.tooltip=Si su país bloquea maps.google.com, selecciona un dominio diferente para la API Google Maps.
jm.webmap.grid_menu_item_text=Chunks
jm.webmap.grid_menu_item_title=Mostrar Chunks
jm.webmap.location_text=Ubicación:
jm.webmap.mobs_menu_item_text=Mobs
jm.webmap.mobs_menu_item_title=Mostrar mobs cercanos
jm.webmap.night_button_text=Noche
jm.webmap.night_button_title=Mapa en modo nocturno.
jm.webmap.pets_menu_item_text=Mascotas
jm.webmap.pets_menu_item_title=Mostrar mascotas cercanas
jm.webmap.players_menu_item_text=Jugadores
jm.webmap.players_menu_item_title=Mostrar jugadores cercanos (multijugador)
jm.webmap.rss_feed_text=Noticias RSS
jm.webmap.rss_feed_title=Suscribirse a las noticias RSS de JourneyMap
jm.webmap.save_button_text=Guardar Mapa
jm.webmap.save_button_title=Guardar el mapa explorado en un archivo de imagen.
jm.webmap.save_filename=Guardando el mapa en el archivo '%1$s'
jm.webmap.show_menu_text=Mostrar...
jm.webmap.slice_text=Parte vertical:
jm.webmap.title=Página del mapa
jm.webmap.topo_button_text=Topográfico
jm.webmap.topo_button_title=Mapa topográfico de la superficie
jm.webmap.update_button_title=Actualización disponible: JourneyMap %1$s para Minecraft %2$s
jm.webmap.villagers_menu_item_text=Aldeanos
jm.webmap.villagers_menu_item_title=Mostrar aldeanos cercanos
jm.webmap.waypoints_menu_item_text=Marcas
jm.webmap.waypoints_menu_item_title=Mostrar Marcas
jm.webmap.web_link_text=journeymap.info
jm.webmap.web_link_title=Página web de JourneyMap
jm.webmap.worldname_text=Nombre del mundo:
jm.webmap.worldtime_text=Hora del mundo:
jm.webmap.zoom_slider_name=Zoom

key.journeymap.create_waypoint=Crear Marca
key.journeymap.day=Mostrar el mapa diurno
key.journeymap.map_toggle=Mapa Completo
key.journeymap.map_toggle_alt=Mostrar/Ocultar Mapa Completo
key.journeymap.minimap_toggle_alt=Mostrar/Ocultar MiniMapa
key.journeymap.minimap_preset=Alternar Diseño de MiniMapa
key.journeymap.minimap_type=Cambiar el tipo de Mapa
key.journeymap.zoom_in=Acercar mapa
key.journeymap.zoom_out=Alejar mapa
key.journeymap.fullscreen_actions=Administrar Acciones
key.journeymap.fullscreen_options=Abrir la Configuración
key.journeymap.fullscreen_waypoints=Administrar Marcas
key.journeymap.fullscreen.north=Desplazar al Norte (16 bloques)
key.journeymap.fullscreen.south=Desplazar al Sur (16 bloques)
key.journeymap.fullscreen.east=Desplazar al Este (16 bloques)
key.journeymap.fullscreen.west=Desplazar al Oeste (16 bloques)
key.journeymap.fullscreen_create_waypoint=Crear Marca
key.journeymap.fullscreen_chat_position=Publicar en el chat la posición
